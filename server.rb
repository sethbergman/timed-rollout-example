require 'webrick'
require 'securerandom'

server = WEBrick::HTTPServer.new :Port => ENV.fetch('PORT'){ 5000 }

#Return a successful Hello World response.
server.mount_proc '/' do |_request, response|
  response.status = 200
  response["Content-Type"] = "text/html"
  response.body = ["<html><body><div style='width:800px; margin:0 auto;'><h1>GitLab Timed Rollout example!</h1></div></body></html>"]
end

#Return an error to help demonstrate monitoring capabilities
server.mount_proc '/error' do |_request, response|
  response.status = 500
  response.body = 'Sorry we encountered an error.'
  sleep 1.0+SecureRandom.random_number
end

server.start
