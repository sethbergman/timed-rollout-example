image: alpine:latest

variables:
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""  # https://gitlab.com/gitlab-org/gitlab-runner/issues/4501
  ROLLOUT_RESOURCE_TYPE: deployment
  INCREMENTAL_ROLLOUT_MODE: timed

stages:
  - build
  - timed rollout 30%
  - timed rollout 60%
  - timed rollout 100%
  - production

.auto-deploy:
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-deploy-image:v0.4.0"

build:
  stage: build
  image: "registry.gitlab.com/gitlab-org/cluster-integration/auto-build-image/master:stable"
  variables:
    DOCKER_TLS_CERTDIR: ""
  services:
    - docker:stable-dind
  script:
    - |
      if [[ -z "$CI_COMMIT_TAG" ]]; then
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE/$CI_COMMIT_REF_SLUG}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_SHA}
      else
        export CI_APPLICATION_REPOSITORY=${CI_APPLICATION_REPOSITORY:-$CI_REGISTRY_IMAGE}
        export CI_APPLICATION_TAG=${CI_APPLICATION_TAG:-$CI_COMMIT_TAG}
      fi
    - /build/build.sh
  only:
    - branches
    - tags

.production: &production_template
  extends: .auto-deploy
  stage: production
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy
    - auto-deploy delete rollout
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths: [environment_url.txt]

production:
  <<: *production_template
  only:
    refs:
      - master
    kubernetes: active
  except:
    variables:
      - $INCREMENTAL_ROLLOUT_ENABLED
      - $INCREMENTAL_ROLLOUT_MODE

.rollout: &rollout_template
  extends: .auto-deploy
  script:
    - auto-deploy check_kube_domain
    - auto-deploy download_chart
    - auto-deploy ensure_namespace
    - auto-deploy initialize_tiller
    - auto-deploy create_secret
    - auto-deploy deploy rollout $ROLLOUT_PERCENTAGE
    - auto-deploy scale stable $((100-ROLLOUT_PERCENTAGE))
    - auto-deploy persist_environment_url
  environment:
    name: production
    url: http://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  artifacts:
    paths: [environment_url.txt]

.timed_rollout_template: &timed_rollout_template
  <<: *rollout_template
  when: delayed
  start_in: 1 minutes
  only:
    refs:
      - master
    kubernetes: active
    variables:
      - $INCREMENTAL_ROLLOUT_MODE == "timed"

timed rollout 30%:
  <<: *timed_rollout_template
  stage: timed rollout 30%
  variables:
    ROLLOUT_PERCENTAGE: 30

timed rollout 60%:
  <<: *timed_rollout_template
  stage: timed rollout 60%
  variables:
    ROLLOUT_PERCENTAGE: 60

timed rollout 100%:
  <<: *timed_rollout_template
  <<: *production_template
  stage: timed rollout 100%
  variables:
    ROLLOUT_PERCENTAGE: 100
